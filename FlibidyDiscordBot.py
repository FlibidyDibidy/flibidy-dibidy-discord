# FlibidyDiscordBot
# Original by 0xDEADCADE
# Updated by FlibidyDibidy

import os
import json
import requests
import uuid
import mariadb
import asyncio
from urllib import parse

import discord

HELPING_EMOJI = "🦈"
NO_HELPING_EMOJI = "🐟"
SETTINGS_PATH = "./settings.json"
CURRENT_TASK = "https://www.flibidydibidy.com/cta/where"

# Check for and open settings
if os.path.exists(SETTINGS_PATH):
    with open(SETTINGS_PATH, "r") as f:
        settings = json.load(f)
else:
    print(f"Please create and fill the '{SETTINGS_PATH}' file.")
    exit()

# Connect to Database
try:
    conn = mariadb.connect(
            user=settings["DB_USER"],
            password=settings["DB_PASSWORD"],
            host=settings["DB_HOST"],
            port=settings["DB_PORT"],
            database=settings["DB_DATABASE"])
except mariadb.Error as e:
    print(e)
    sys.exit(1)
cur = conn.cursor()


def welcome_message(token, name):
    message = f"*Thank you!* Please use the following link to help with this task:\n"
    url = f"{CURRENT_TASK}?" + parse.urlencode({'token': token, 'name': name})
    message += f"{url}\n"
    message += f"Please do not share this link, as your token is unique to you!"

    return message





# The BOT
class MyClient(discord.Client):
    async def on_ready(self):
        print("Ready ", str(client.user))
        self.channel = await client.fetch_channel(settings["DISCORD_HELPER_CHANNEL_ID"])
        self.rate_limited_users = []

    async def on_raw_reaction_add(self, payload):
        global settings
        if payload.user_id == client.user.id:
            return

        if payload.user_id in self.rate_limited_users:
            return

        if payload.channel_id == settings["DISCORD_HELPER_CHANNEL_ID"] and str(payload.emoji) == HELPING_EMOJI:
            await payload.member.add_roles(discord.Object(settings["DISCORD_HELPER_ROLE_ID"]))

            try:
                conn.ping()
            except mariadb.InterfaceError:
                conn.reconnect()

            token = None
            cur.execute("SELECT token FROM contributor WHERE discord_user_id = ?", (payload.user_id,))
            for v in cur.fetchall():
                token = v[0]

            if token is None:
                token = str(uuid.uuid4())[-12:]
                cur.execute(
                """INSERT INTO contributor (token, discord_user_id, discord_user_name, discord_avatar_url) 
                   VALUES (?, ?, ?, ?)""",
                   (token, payload.user_id, str(payload.member).encode("utf-8"), str(payload.member.avatar_url)))
                conn.commit()
                print(f"NEW CONTRIBUTOR: {str(payload.member)}")

            try:
                await payload.member.send(welcome_message(token, str(payload.member)))
            except Exception as e:
                await self.channel.send(f"{payload.member.mention} Please enable messages from server members.\nServer Settings>Privacy Settings>Allow direct messages from server members",
                delete_after=30);

            self.rate_limited_users.append(payload.user_id)
            await asyncio.sleep(20)
            self.rate_limited_users.remove(payload.user_id)
        elif payload.channel_id == settings["DISCORD_HELPER_CHANNEL_ID"] and str(payload.emoji) == NO_HELPING_EMOJI:
            print(f"remove helper role: {str(payload.member)}")
            await payload.member.remove_roles(discord.Object(settings["DISCORD_HELPER_ROLE_ID"]))
            msg = await self.channel.fetch_message(payload.message_id)
            await msg.remove_reaction(NO_HELPING_EMOJI, payload.member)


client = MyClient()
client.run(settings["DISCORD_TOKEN"])
