# Flibidy-Dibidy-Discord
The bot for authenticating / managing video helpers


## Credit
Initial prototype from: 0xDEADCADE: https://github.com/0xDEADCADE/FlibidyDibidyAuthBot

## Setup
```
sudo apt-get install mariadb-client pip libmariadb-dev
pip3 install mariadb discord
```
